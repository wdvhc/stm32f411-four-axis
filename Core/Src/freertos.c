/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "sys.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
osThreadId KEYTaskHandle;
osThreadId OLEDTaskHandle;
osThreadId NRFTaskHandle;
osThreadId mpuTaskHandle;
osThreadId CONTaskHandle;
osThreadId UARTTaskHandle;
osMutexId wdvhcMutexHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void KEYTaskFunction(void const * argument);
void OLEDTaskFunction(void const * argument);
void NRFTaskFunction(void const * argument);
void mpuTaskFunction(void const * argument);
void CONTaskFunction(void const * argument);
void UARTTaskFunction(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */
  /* Create the mutex(es) */
  /* definition and creation of wdvhcMutex */
  osMutexDef(wdvhcMutex);
  wdvhcMutexHandle = osMutexCreate(osMutex(wdvhcMutex));

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of KEYTask */
  osThreadDef(KEYTask, KEYTaskFunction, osPriorityLow, 0, 128);
  KEYTaskHandle = osThreadCreate(osThread(KEYTask), NULL);

  /* definition and creation of OLEDTask */
  osThreadDef(OLEDTask, OLEDTaskFunction, osPriorityIdle, 0, 128);
  OLEDTaskHandle = osThreadCreate(osThread(OLEDTask), NULL);

  /* definition and creation of NRFTask */
  osThreadDef(NRFTask, NRFTaskFunction, osPriorityBelowNormal, 0, 256);
  NRFTaskHandle = osThreadCreate(osThread(NRFTask), NULL);

  /* definition and creation of mpuTask */
  osThreadDef(mpuTask, mpuTaskFunction, osPriorityNormal, 0, 1024);
  mpuTaskHandle = osThreadCreate(osThread(mpuTask), NULL);

  /* definition and creation of CONTask */
  osThreadDef(CONTask, CONTaskFunction, osPriorityNormal, 0, 2048);
  CONTaskHandle = osThreadCreate(osThread(CONTask), NULL);

  /* definition and creation of UARTTask */
  osThreadDef(UARTTask, UARTTaskFunction, osPriorityBelowNormal, 0, 256);
  UARTTaskHandle = osThreadCreate(osThread(UARTTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_KEYTaskFunction */
/**
  * @brief  Function implementing the KEYTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_KEYTaskFunction */
void KEYTaskFunction(void const * argument)
{
  /* USER CODE BEGIN KEYTaskFunction */
	//uint8_t keytime = 0;
  /* Infinite loop */
  for(;;)
  {
		/*
		keytime++;
		key_read();
		
		if(wdvhc_error_flag == 1)
		{
			if(keytime == 2)
			{
				HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);
				keytime = 0;
			}
		}
		if(wdvhc_24_error_flag == 1)
		{
			if(keytime == 10)
			{
				HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);
				keytime = 0;
			}
		}
		else
		{
			if(keytime == 100)
			{
				HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);
				keytime = 0;
			}
		}
		key_read_flag();
		*/
		while(BMP280_GetStatus(BMP280_MEASURING) != RESET);
		while(BMP280_GetStatus(BMP280_IM_UPDATE) != RESET);
		BMP_Temperature = BMP280_Get_Temperature();
		BMP_Pressure = BMP280_Get_Pressure();
    osDelay(10);
  }
  /* USER CODE END KEYTaskFunction */
}

/* USER CODE BEGIN Header_OLEDTaskFunction */
/**
* @brief Function implementing the OLEDTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_OLEDTaskFunction */
void OLEDTaskFunction(void const * argument)
{
  /* USER CODE BEGIN OLEDTaskFunction */
  /* Infinite loop */
  for(;;)
  {
		OLED_ShowNum(1, 1, YML, 3);
		OLED_ShowNum(2, 1, FYL, 2);
		OLED_ShowChar(2, 3,'%');
		OLED_ShowNum(3, 1, HGL, 2);
		OLED_ShowChar(3, 3, '%');	
		
		OLED_ShowNum(4, 16, nrf_rx, 1);

		OLED_ShowSignedNum(1, 13, Pitch, 3);
		OLED_ShowSignedNum(2, 13, Roll, 3);
		OLED_ShowSignedNum(3, 13, Yaw, 3);
		
		OLED_ShowNum(1, 5, wdvhc_24_error_time, 4);
		OLED_ShowHexNum(2,5, wdvhc_24_error_flag,1);
		OLED_ShowHexNum(3,5, wdvhc_error_flag,1);
		OLED_ShowNum(4, 6, Rx_ok_Flag, 1);
		OLED_ShowNum(4, 14, YML_flag, 1);
		OLED_ShowSignedNum(3, 7, Gyro_x_Kd, 3);
		OLED_ShowNum(4, 1, (int)NOW_Alt, 4);
		Rx_ok_Flag = 0;
		
    osDelay(20);
  }
  /* USER CODE END OLEDTaskFunction */
}

/* USER CODE BEGIN Header_NRFTaskFunction */
/**
* @brief Function implementing the NRFTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_NRFTaskFunction */
void NRFTaskFunction(void const * argument)
{
  /* USER CODE BEGIN NRFTaskFunction */
  /* Infinite loop */
  for(;;)
  {
		nrf_rx = nrf24l01_rx_packet(nrf_buff);
		if(nrf_rx == 0)
		{
			//��û�����
			if(osMutexWait(wdvhcMutexHandle,osWaitForever)==osOK)
			{
				YML_Last = YML;
				YML = (short)(nrf_buff[0]<<8) | nrf_buff[1];
				
				//������
				if(YML - YML_Last>2)
					YML_flag = 1;
				//�½���
				else if(YML_Last-YML>2)
					YML_flag = 0;

				HGL = (short)(nrf_buff[2]<<8) | nrf_buff[3];
				FYL = (short)(nrf_buff[4]<<8) | nrf_buff[5];
				PHL = (short)((nrf_buff[6]<<8) | nrf_buff[7]);
				
				GL_flag = nrf_buff[8];
				
				//�ͷŻ�����
				osMutexRelease(wdvhcMutexHandle);
			}
		}
    osDelay(50);
  }
  /* USER CODE END NRFTaskFunction */
}

/* USER CODE BEGIN Header_mpuTaskFunction */
/**
* @brief Function implementing the mpuTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_mpuTaskFunction */
void mpuTaskFunction(void const * argument)
{
  /* USER CODE BEGIN mpuTaskFunction */
	uint8_t mputime = 0;
  /* Infinite loop */
  for(;;)
  {
		mputime++;
		if(mputime == 4)
		{
			wdvhc_get_data(1);
			mputime = 0;
		}
		else
			wdvhc_get_data(0);
		
    osDelay(2);
  }
  /* USER CODE END mpuTaskFunction */
}

/* USER CODE BEGIN Header_CONTaskFunction */
/**
* @brief Function implementing the CONTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_CONTaskFunction */
void CONTaskFunction(void const * argument)
{
  /* USER CODE BEGIN CONTaskFunction */
	uint8_t contime = 0;
	osDelay(2000);
  /* Infinite loop */
  for(;;)
  {
		contime++;
		
		if(wdvhc_24_error_flag != 1)
		{
			if(fly_out_ztj == 0)
			{
				YML_pwm = YML;
			}
			
			if(GL_flag == 1)
			{
				EK_Dir_x_SP = (FYL-49);
				EK_Dir_y_SP = (HGL-50);
			}
			else
			{
				FYL_Gyro = (FYL-49)/5;
				HGL_Gyro = (HGL-50)/5;
			}
		}
		else
		{
			FYL_Gyro = 0;
			HGL_Gyro = 0;
		}
		
		Control_Function(contime);
		if(contime == 3)
			contime = 0;
		
    osDelay(4);
  }
  /* USER CODE END CONTaskFunction */
}

/* USER CODE BEGIN Header_UARTTaskFunction */
/**
* @brief Function implementing the UARTTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_UARTTaskFunction */
void UARTTaskFunction(void const * argument)
{
  /* USER CODE BEGIN UARTTaskFunction */
	//float axx[3];
	uint8_t xczx = 0xff;
  /* Infinite loop */
  for(;;)
  {
		xczx = ~xczx;
		
    printf("%f,%f,%f\n", (float)Gyro[0]* 0.0609756f, (float)Gyro[1]* 0.0609756f, (float)Gyro[2]* 0.0609756f);                                                                                                                                                                                      
		osDelay(10);
  }
  /* USER CODE END UARTTaskFunction */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
/* USER CODE END Application */
