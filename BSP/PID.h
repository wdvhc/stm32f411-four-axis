#ifndef __PID_H
#define __PID_H

#include "sys.h"

extern float iiii;

extern float DG_x_Kp;
extern float DG_y_Kp;
extern float DG_z_Kp;

extern float DG_x_Ki;
extern float DG_y_Ki;
extern float DG_z_Ki;

extern float DG_x_Kd;
extern float DG_y_Kd;
extern float DG_z_Kd;

extern float Gyro_x_Kp;
extern float Gyro_y_Kp;
extern float Gyro_z_Kp;

extern float Gyro_x_Ki;
extern float Gyro_y_Ki;
extern float Gyro_z_Ki;

extern float Gyro_x_Kd;
extern float Gyro_y_Kd;
extern float Gyro_z_Kd;

float DG_PID_X(float Ek_Distance, float Now_Distance);
float DG_PID_Y(float Ek_Distance, float Now_Distance);
float DG_PID_Z(float Ek_Distance, float Now_Distance);

float Gyro_PID_X(float Ek_Distance, float Now_Distance);
float Gyro_PID_Y(float Ek_Distance, float Now_Distance);
float Gyro_PID_Z(float Ek_Distance, float Now_Distance);

extern float Altitude_Kp;
extern float Altitude_Ki;
extern float Altitude_Kd;
extern float Alt_SP_WZS_Kp;
extern float Alt_SP_WZS_Ki;
extern float Alt_SP_WZS_Kd;
float Altitude_PID(float Ek_Distance, float Now_Distance);
float Alt_SP_WZS_curb(float Ek_Distance, float Now_Distance);

extern float Direct_X_Kp;
extern float Direct_X_Ki;
extern float Direct_X_Kd;
extern float Direct_Y_Kp;           
extern float Direct_Y_Ki;            
extern float Direct_Y_Kd;
extern float Direct_X_SP_Kp;
extern float Direct_X_SP_Kd;
extern float Direct_Y_SP_Kp;
extern float Direct_Y_SP_Kd;

float Direct_X_PID(float Ek_Distance, float Now_Distance);
float Direct_Y_PID(float Ek_Distance, float Now_Distance);
float Direct_X_SPcurb(float Ek_Speed, float Now_Speed);
float Direct_Y_SPcurb(float Ek_Speed, float Now_Speed);

extern float ACC_Z_Kp;
extern float ACC_Z_Kd;
float ACC_Z_PID(float Ek_Distance, float Now_Distance);

extern float Gyro_Alt_Kp;
float Gyro_Alt_PID(float Pitch_Now_Distance, float Roll_Now_Distance);

#endif
