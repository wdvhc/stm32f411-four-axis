#ifndef __SPI_H
#define __SPI_H

#include "sys.h"

//CS   PA4
//CLK  PA5
//MISO PA6
//MOSI PA7
/***********************************************************************/
#define SPI_CS_PORT      GPIOA
#define SPI_CS_PIN       GPIO_PIN_4

#define SPI_SCK_PORT     GPIOA
#define SPI_SCK_PIN      GPIO_PIN_5

#define SPI_MOSI_PORT    GPIOA
#define SPI_MOSI_PIN     GPIO_PIN_7

#define SPI_MISO_PORT    GPIOA
#define SPI_MISO_PIN     GPIO_PIN_6
/***********************************************************************/

/***********************************************************************/
#define SPI_CS_LOW      SPI_CS_PORT->BSRR = ((uint32_t)SPI_CS_PIN<<16U)
#define SPI_CS_HIGH     SPI_CS_PORT->BSRR = SPI_CS_PIN

#define SPI_SCK_LOW     SPI_SCK_PORT->BSRR = ((uint32_t)SPI_SCK_PIN<<16U)
#define SPI_SCK_HIGH    SPI_SCK_PORT->BSRR = SPI_SCK_PIN

#define SPI_MISO_LOW		SPI_MISO_PORT->BSRR = ((uint32_t)SPI_MISO_PIN<<16U)		//GPIOx->BSRR = GPIO_Pin;
#define SPI_MISO_HIGH   SPI_MISO_PORT->BSRR = SPI_MISO_PIN										//GPIOx->BSRR = ((uint32_t)GPIO_Pin << 16U);
#define SPI_MISO_READ   SPI_MISO_PORT->IDR & SPI_MISO_PIN											//GPIOx->IDR & GPIO_Pin;

#define SPI_MOSI_LOW    SPI_MOSI_PORT->BSRR = ((uint32_t)SPI_MOSI_PIN<<16U)
#define SPI_MOSI_HIGH   SPI_MOSI_PORT->BSRR = SPI_MOSI_PIN
/*************************************************************************/
/*
#define SPI_CS_LOW      HAL_GPIO_WritePin(SPI_CS_PORT, SPI_CS_PIN, GPIO_PIN_RESET)			//SPI_CS_PORT->BSRR = SPI_CS_PIN
#define SPI_CS_HIGH     HAL_GPIO_WritePin(SPI_CS_PORT, SPI_CS_PIN, GPIO_PIN_SET)				//SPI_CS_PORT->BSRR = ((uint32_t)SPI_CS_PIN<<16U)

#define SPI_SCK_LOW     HAL_GPIO_WritePin(SPI_SCK_PORT, SPI_SCK_PIN, GPIO_PIN_RESET)		//SPI_SCK_PORT->BSRR = SPI_SCK_PIN
#define SPI_SCK_HIGH    HAL_GPIO_WritePin(SPI_SCK_PORT, SPI_SCK_PIN, GPIO_PIN_SET)			//SPI_SCK_PORT->BSRR = ((uint32_t)SPI_SCK_PIN<<16U)

#define SPI_MISO_LOW    HAL_GPIO_WritePin(SPI_MISO_PORT, SPI_MISO_PIN, GPIO_PIN_RESET)	//SPI_MISO_PORT->BSRR = ((uint32_t)SPI_MISO_PIN<<16U)		//GPIOx->BSRR = GPIO_Pin;
#define SPI_MISO_HIGH   HAL_GPIO_WritePin(SPI_MISO_PORT, SPI_MISO_PIN, GPIO_PIN_SET)		//SPI_MISO_PORT->BSRR = SPI_MISO_PIN										//GPIOx->BSRR = ((uint32_t)GPIO_Pin << 16U);
#define SPI_MISO_READ   HAL_GPIO_ReadPin(SPI_MISO_PORT, SPI_MISO_PIN)										//SPI_MISO_PORT->IDR & SPI_MISO_PIN											//GPIOx->IDR & GPIO_Pin;

#define SPI_MOSI_LOW    HAL_GPIO_WritePin(SPI_MOSI_PORT, SPI_MOSI_PIN, GPIO_PIN_RESET)	//SPI_MOSI_PORT->BSRR = SPI_MOSI_PIN
#define SPI_MOSI_HIGH   HAL_GPIO_WritePin(SPI_MOSI_PORT, SPI_MOSI_PIN, GPIO_PIN_SET)		//SPI_MOSI_PORT->BSRR = ((uint32_t)SPI_MOSI_PIN<<16U)
*/
/***********************************************************************/

void spi_Init(void);
uint8_t SPI_ReadWrite_Byte(uint8_t tx_data);

#endif
