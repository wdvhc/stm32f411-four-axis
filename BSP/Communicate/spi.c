#include "spi.h"

//CS   PA4
//CLK  PA5
//MISO PA6
//MOSI PA7

void spi_Init(void)
{
	SPI_CS_HIGH;
	SPI_SCK_LOW;
}

/*
初始时钟默认低电平，第一个沿发送
读写一个字节
*/
uint8_t SPI_ReadWrite_Byte(uint8_t tx_data)
{
	uint8_t i;
	
  //先将时钟线拉低，确保一开始是默认电平
  //SPI_SCK_LOW;
	
	for(i=0; i<8; i++)
	{
		//判断数据
		if((tx_data & 0x80) == 0x80)
			SPI_MOSI_HIGH;
		else
			SPI_MOSI_LOW;
		
		//上升沿发送
		//将时钟线拉高，在时钟上升沿，数据发送到从设备
		SPI_SCK_HIGH;
		
		tx_data<<=1;
				
		//读取从设备发送过来的数据
		if(SPI_MISO_READ)
			tx_data |= 0x01;
		
	  //在下降沿数据被读取到主机
		SPI_SCK_LOW;		
	}
	
  //返回读取到的数据
  return tx_data;
}

/*
uint8_t SPI_ReadWrite_Byte(uint8_t tx_data)
{
	uint8_t i,rx_data = 0x00;
	
	for(i=0; i<8; i++)
	{
		//判断数据
		if((tx_data & (0x80>>i)) == (0x80>>i))
			SPI_MOSI_HIGH;
		else
			SPI_MOSI_LOW;
		
		//上升沿发送
		//将时钟线拉高，在时钟上升沿，数据发送到从设备
		SPI_SCK_HIGH;
				
		//读取从设备发送过来的数据
		if((SPI_MISO_READ) == 1)
			rx_data |= (0x80>>i);
		
	  //在下降沿数据被读取到主机
		SPI_SCK_LOW;
	}
	
  //返回读取到的数据
  return rx_data;
}
*/

