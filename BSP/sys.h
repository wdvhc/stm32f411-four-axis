#ifndef __SYS_H
#define __SYS_H

#include "main.h"
#include "math.h"
#include "stdio.h"
#include "string.h"
#include "oled.h"
#include "i2c.h"
#include "mpu6050.h"
#include "wdvhc_IMU.h"
#include "spi.h"
#include "NRF24L01.h"
#include "PID.h"
#include "bmp280.h"

#include "cmsis_os.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

#define LPF_1_(hz,t,in,out) ((out) += ( 1 / ( 1 + 1 / ( (hz) *3.14f *(t) ) ) ) *( (in) - (out) ))

//悬停角度
#define XT_Gyro_x  -0.42f
#define XT_Gyro_y  -1.41f

//悬停高度
#define XT_Alt_GD 500

//限制上升速度
#define fly_climb 4
//限制下降速度
#define fly_down -4

extern double BMP_Pressure,BMP_Temperature;

extern uint8_t Gyro_mode_flag;
extern uint8_t Gyro_Alt_mode_flag;
extern uint8_t Altitude_mode_flag;
extern uint8_t Direct_Alt_mode_flag;
extern uint8_t fly_flag;
extern uint8_t fly_out_ztj;
extern uint16_t fly_curb_time;
extern uint8_t Altitude_flag;
extern uint8_t wdvhc_24_error_flag;
extern uint16_t wdvhc_24_error_time;
extern uint8_t wdvhc_error_flag;

extern uint8_t wdvhc_GLZL;
extern float NOW_DW_buff[];
extern float NOW_DW_buff_LPF_1[];
extern float NOW_DW_buff_RHRH[];
extern float NOW_DW_buff_CSCS[];
extern float EK_Alt_SP;
extern float NOW_Alt_SP;
//extern float NOW_Alt_SP_Last;
//extern float RH_AltZ_SP;
extern float EK_Alt;
extern float YUANSHI_Alt;
extern float NOW_Alt;
extern float NOW_Alt_Last;
extern uint8_t NOW_Alt_time;

extern uint8_t GL_flag;
extern float EK_Dir_x;
extern float EK_Dir_y;
extern float NOW_Dir_x;
extern float NOW_Dir_y;
extern float NOW_Dir_x_Last;
extern float NOW_Dir_y_Last;
extern float EK_Dir_x_SP;
extern float EK_Dir_y_SP;
extern float NOW_Dir_x_SP;
extern float NOW_Dir_y_SP;
extern float NOW_Dir_x_SPLAST;
extern float NOW_Dir_y_SPLAST;
extern float BCBC_Dir_x_SP;
extern float BCBC_Dir_y_SP;

extern float EK_x_DG;
extern float EK_y_DG;
extern float EK_z_DG;
extern float EK_x_Gyro;
extern float EK_y_Gyro;
extern float EK_z_Gyro;

extern uint16_t pwmzs;
extern uint16_t pwmys;
extern uint16_t pwmzx;
extern uint16_t pwmyx;

extern int YML_pwm;
extern int FYL_Gyro;
extern int HGL_Gyro;

extern float DGCon_x_pwm;
extern float DGCon_y_pwm;
extern float DGCon_z_pwm;

extern float Alt_pwm;
extern float ACC_Z_pwm;
extern float twoM_Alt_pwm;
extern float Gyro_Alt_pwm;
extern float Dir_x_Gyro;
extern float Dir_y_Gyro;

extern uint8_t nrf_rx;
extern uint8_t nrf_buff[];

extern int YML;
extern int YML_Last;
extern uint8_t YML_flag;

extern int FYL;
extern int HGL;
extern int PHL;

extern uint8_t Key_S0;
extern uint8_t Key_S1;
extern uint8_t Key_S2;
extern uint8_t Key_S3;

extern uint8_t Key_S0_flag;
extern uint8_t Key_S1_flag;
extern uint8_t Key_S2_flag;
extern uint8_t Key_S3_flag;

extern uint8_t Rx_i;
extern uint8_t Rx_String[];  //接收字符串数组
extern uint8_t Rx_Flag;      //接收字符串计数
extern uint8_t Rx_buff;     	//接收缓存
extern uint8_t Rx_ok_Flag;		//接收成功标志位

/*
extern float wdvhc_acc;
extern float wdvhc_acc_last;
extern float wdvhc_ZSP;
extern float wdvhc_ZSP_pwm;
extern uint8_t _ZSP_time;
*/

void ZZZ_SP_RH(void);
void Control_Function(uint8_t time);
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);
int fputc(int ch, FILE *f);
int fgetc(FILE *f);
int KalmanFilter(int inData);
float wdvhc_abs(float num);
void key_read(void);
void key_read_flag(void);

float FastSin(float x);
float FastCos(float x);


#endif
