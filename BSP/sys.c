#include "sys.h"

//4顺    X    1逆
//
//			口		 Y
//
//3逆         2顺

//气压计
double BMP_Pressure,BMP_Temperature;
/***2.4H****/
uint8_t nrf_rx = 0;
uint8_t nrf_buff[32] = {0};

//油门量
int YML=0;
int YML_Last=0;
//油门flag, 0表示最近一次油门边沿是下降沿， 1表示最近一次油门边沿是上升沿
uint8_t YML_flag = 0;

//俯仰量
int FYL=0;
//横滚量
int HGL=0;
//偏航量
int PHL=0;

/***姿态期望***/
//纯姿态油门模式
uint8_t Gyro_mode_flag =  1;
//姿态高度模式
uint8_t Gyro_Alt_mode_flag = 	0;
//定高模式标志位
uint8_t Altitude_mode_flag = 0;
//定点悬停模式标志位
uint8_t Direct_Alt_mode_flag = 0;

//起飞降落标志
uint8_t fly_flag  = 0;
//起飞降落状态机
uint8_t fly_out_ztj = 0;
uint16_t fly_curb_time = 0;

//定高标志位
//uint8_t Altitude_flag = 0;

//遥控器信号丢失
uint8_t wdvhc_24_error_flag = 0;
uint16_t wdvhc_24_error_time = 0;
//姿态超调错误
uint8_t wdvhc_error_flag = 0;

//期望角速度
float EK_x_DG = 0;
float EK_y_DG = 0;
float EK_z_DG = 0;

//期望角度
float EK_x_Gyro = 0;
float EK_y_Gyro = 0;
float EK_z_Gyro = 0;

//原始数据
//0高度, 12 xy
float NOW_DW_buff[3];

float NOW_DW_buff_LPF_1[3];

float NOW_DW_buff_RHRH[3];

float NOW_DW_buff_CSCS[3];

//期望高度
float EK_Alt = 520;

//IIR低通滤波后的数据
float YUANSHI_Alt = 0;
//角度互补
//实际高度
float NOW_Alt = 0;
//上次实际高度
float NOW_Alt_Last = 0;

//期望上升下降速度
float EK_Alt_SP = 0;
//实际上升下降速度
float NOW_Alt_SP = 0;

uint8_t NOW_Alt_time = 0;

//光流质量
//越大越好
uint8_t wdvhc_GLZL = 0;

//光流是否开启标志位
uint8_t GL_flag = 0;

//期望xy
float EK_Dir_x = 0;
float EK_Dir_y = 0;

//实际xy
float NOW_Dir_x = 0;
float NOW_Dir_y = 0;
//实际xy
float NOW_Dir_x_Last = 0;
float NOW_Dir_y_Last = 0;

//期望xy速度
float EK_Dir_x_SP = 0;
float EK_Dir_y_SP = 0;
//实际xy速度
float NOW_Dir_x_SP = 0;
float NOW_Dir_y_SP = 0;

float NOW_Dir_x_SPLAST = 0;
float NOW_Dir_y_SPLAST = 0;

float NOW_Dir_x_flag = 0;
float NOW_Dir_y_flag = 0;
//角速度补偿后xy速度
float BCBC_Dir_x_SP = 0;
float BCBC_Dir_y_SP = 0;

/***四轴pwm***/
//输出pwm
uint16_t pwmzs = 0;
uint16_t pwmys = 0;
uint16_t pwmzx = 0;
uint16_t pwmyx = 0;

//油门量pwm
int YML_pwm = 0;
int FYL_Gyro = 0;
int HGL_Gyro = 0;

//角速度环pwm
float DGCon_x_pwm = 0;
float DGCon_y_pwm = 0;
float DGCon_z_pwm = 0;

//位置环xy角度
float Dir_x_Gyro = 0;
float Dir_y_Gyro = 0;

//高度环pwm
float Alt_pwm = 0;

//降落保护pwm
float ACC_Z_pwm = 0;

//2米保护pwm
float twoM_Alt_pwm = 0;

//倾角油门补偿
float Gyro_Alt_pwm = 0;

/*
uint8_t _ZSP_time = 0;
//16384f
//z轴加速度
float wdvhc_acc=0;
float wdvhc_acc_last=0;

//z轴速度
float wdvhc_ZSP=0;
//z轴加速度补偿pwm
float wdvhc_ZSP_pwm=0;
*/

uint16_t wdvhc_ZSP_time = 0;

//检查
void error_curb_Function(void)
{
	//姿态失控判断
	if(wdvhc_abs(Pitch)>45 || wdvhc_abs(Roll)>45)
		wdvhc_error_flag = 1;
	
	if(wdvhc_24_error_flag == 0)
	{
		//遥控丢失判断
		if(nrf_rx == 1)
			wdvhc_24_error_time ++;
		else
			wdvhc_24_error_time = 0;
		
		if(wdvhc_24_error_time > 500)
		{
			wdvhc_24_error_flag = 1;
			wdvhc_24_error_time = 0;
		}
	}
}

/*
//z轴速度融合
void ZZZ_SP_RH(void)
{
	if(NOW_Alt_SP*wdvhc_ZSP>0 && (NOW_Alt_SP>1.0f || NOW_Alt_SP<-1.0f))
		RH_AltZ_SP =  0.6f*NOW_Alt_SP + 0.4f*wdvhc_ZSP;
	else
	{
		RH_AltZ_SP =  0.6f*NOW_Alt_SP;
	}
}*/

//炸鸡函数
void Gyro_error_flag_Function(void)
{
	if(pwmys+pwmyx+pwmzx+pwmzs > 20)
	{
		//右上
		pwmys -= 3;
		//右下
		pwmyx -= 3;
		//左下
		pwmzx -= 3;
		//左上
		pwmzs -= 3;
	}
	
	//输出
	__HAL_TIM_SetCompare(&htim1, TIM_CHANNEL_1, 1000+pwmys);    //修改比较值，修改占空比
	__HAL_TIM_SetCompare(&htim1, TIM_CHANNEL_2, 1000+pwmyx);    //修改比较值，修改占空比		这个16%油门时，其他三才动
	__HAL_TIM_SetCompare(&htim1, TIM_CHANNEL_3, 1000+pwmzx);    //修改比较值，修改占空比
	__HAL_TIM_SetCompare(&htim1, TIM_CHANNEL_4, 1000+pwmzs);    //修改比较值，修改占空比
}

void Control_Function(uint8_t time)
{
	//错误判断
	error_curb_Function();
	
	//姿态失控,低空炸鸡，高空保持起飞油门，尝试控制
	if(wdvhc_error_flag == 1)
		//默认低空，选择炸鸡
		Gyro_error_flag_Function();
	
	//姿态稳定的情况下
	else
	{
		//进行遥控器信号判断
		//遥控信号丢失
		if(wdvhc_24_error_flag != 0)
		{
			//遥控信号丢失
			//判断是起飞前还是起飞后
			if(fly_flag == 1)
			{
				//起飞后如果是姿态控制模式则原地降落
				YML_pwm = 430;
			}
			else
			{
				//如果没有起飞，则保持0油门
				//输出
				__HAL_TIM_SetCompare(&htim1, TIM_CHANNEL_1, 1000);    //修改比较值，修改占空比
				__HAL_TIM_SetCompare(&htim1, TIM_CHANNEL_2, 1000);    //修改比较值，修改占空比		这个16%油门时，其他三才动
				__HAL_TIM_SetCompare(&htim1, TIM_CHANNEL_3, 1000);    //修改比较值，修改占空比
				__HAL_TIM_SetCompare(&htim1, TIM_CHANNEL_4, 1000);    //修改比较值，修改占空比
				
				return;
			}
		
			//维持两秒
			wdvhc_24_error_time++;
			if(wdvhc_24_error_time >= 500)
			{
				wdvhc_24_error_flag = 2;
				fly_flag = 0;
				YML_pwm = 0;
				//YML = 0;
			}
		}
		//遥控信号没有丢失
		else
		{
			//高度环时间控制
			NOW_Alt_time++;
			
			//判断当前模式，如果是纯姿态模式
			if(Gyro_mode_flag == 1 && Gyro_Alt_mode_flag == 0 && Altitude_mode_flag == 0 && Direct_Alt_mode_flag == 0)
			{
				//油门限幅设定
				if(YML_pwm>=580)
						YML_pwm=580;
				
				//判断有没有起飞
				//还没起飞，置起飞标志后再做姿态控制
				if(fly_flag == 0)
				{
					//大于离地油门后做姿态控制
					if(YML_pwm>=430)
						fly_flag = 1;
				}
				//已经起飞
				else
				{
					//小于定量油门后不做姿态控制
					if(YML_pwm<=200)
						fly_flag = 0;
				}
				
				//如果油门下降, 开启下降保护
				if(fly_flag == 1 && YML_flag == 0)
				{
					//24ms速度控制
					//防止急速下坠
					if(NOW_Alt_time%5 == 0)
					{
						if(NOW_Alt_SP<fly_down)
							ACC_Z_pwm = ACC_Z_PID(fly_down, NOW_Alt_SP);
						else
							ACC_Z_pwm = 0;
					}
				}
				
				if(GL_flag == 1)
				{
					if(NOW_Alt_time%10 == 0)
					{
						//位置环
						EK_Dir_x_SP = Direct_X_PID(EK_Dir_x, NOW_Dir_x);
						EK_Dir_y_SP = Direct_Y_PID(EK_Dir_y, NOW_Dir_y);
					}
					if(NOW_Alt_time%6 == 0)
					{
						//速度环
						Dir_x_Gyro = Direct_X_SPcurb(EK_Dir_x_SP, NOW_Dir_x_SPLAST);
						Dir_y_Gyro = Direct_Y_SPcurb(EK_Dir_y_SP, NOW_Dir_y_SPLAST);
					}
					
					//位置式
					Alt_pwm = Alt_SP_WZS_curb(PHL, NOW_Alt_SP);
					
					//角度融合
					EK_x_Gyro = XT_Gyro_x+Dir_x_Gyro+FYL_Gyro;
					EK_y_Gyro = XT_Gyro_y+Dir_y_Gyro+HGL_Gyro;
				}
				else
				{
					//角度遥控融合
					EK_x_Gyro = XT_Gyro_x+FYL_Gyro;
					EK_y_Gyro = XT_Gyro_y+HGL_Gyro;
				}
			}
			/*
			//高度控制模式
			else if(Gyro_mode_flag == 0 && Gyro_Alt_mode_flag == 1 && Altitude_mode_flag == 0 && Direct_Alt_mode_flag == 0)
			{
				//还没起飞
				if(fly_flag == 0)
				{
					//起飞流程
					if(fly_out_ztj == 0)
					{
						//判断油门
						if(YML_pwm >= 990)
							fly_out_ztj = 1;
						//油门置零
						YML_pwm=0;
					}
					else if(fly_out_ztj == 1)
					{
						YML_pwm++;
						//怠速
						if(YML_pwm >= 300)
							fly_out_ztj = 2;
					}
					else if(fly_out_ztj == 2)
					{
						fly_curb_time++;
						//怠速延时3秒
						if(fly_curb_time >= 1000)
						{
							fly_out_ztj = 3;
							fly_curb_time = 0;
							//起飞油门
							YML_pwm = 460;
							//起飞标志
							fly_flag = 1;
						}
					}
				}
				else
				{
					//高度遥控融合
					EK_Alt = XT_Alt_GD+PHL;
					//20ms
					if(fly_curb_time == 5)
						//速度环
						Alt_pwm = Alt_SPcurb(EK_Alt_SP, RH_AltZ_SP);
					//40ms
					else if(fly_curb_time == 10)
					{
						//位置环
						EK_Alt_SP = Altitude_PID(EK_Alt, RH_AltZ_SP);
						//速度环
						Alt_pwm = Alt_SPcurb(EK_Alt_SP, RH_AltZ_SP);
						
						fly_curb_time = 0;
					}
					fly_curb_time++;
					
					//角度遥控融合
					EK_x_Gyro = XT_Gyro_x+FYL_Gyro;
					EK_y_Gyro = XT_Gyro_y+HGL_Gyro;
				}
			}
			//定高模式
			else if(Gyro_mode_flag == 0 && Gyro_Alt_mode_flag == 0 && Altitude_mode_flag == 1 && Direct_Alt_mode_flag == 0)
			{
				//还没起飞
				if(fly_flag == 0)
				{
					//起飞流程
					if(fly_out_ztj == 0)
					{
						//判断油门
						if(YML_pwm >= 520)
							fly_out_ztj = 1;
						//油门置零
						YML_pwm=0;
					}
					else if(fly_out_ztj == 1)
					{
						YML_pwm++;
						//怠速
						if(YML_pwm >= 200)
							fly_out_ztj = 2;
					}
					else if(fly_out_ztj == 2)
					{
						fly_curb_time++;
						//怠速延时3秒
						if(fly_curb_time >= 1000)
						{
							fly_out_ztj = 3;
							fly_curb_time = 0;
							//起飞油门
							YML_pwm = 460;
							//起飞标志
							fly_flag = 1;
						}
					}
				}
				else
				{
					if(fly_curb_time == 5)
					{
						//速度环
						Alt_pwm = Alt_SPcurb(EK_Alt_SP, RH_AltZ_SP);
					}
					else if(fly_curb_time == 10)
					{
						//位置环
						EK_Alt_SP = Altitude_PID(EK_Alt, NOW_Alt);
						//速度环
						Alt_pwm = Alt_SPcurb(EK_Alt_SP, RH_AltZ_SP);
						fly_curb_time = 0;
					}
					fly_curb_time++;
					
					//切手动模式
					if(YML <= 500)
					{
						Alt_pwm = 0;
						fly_out_ztj = 0;
						YML_pwm = YML;
						Altitude_mode_flag = 0;
						Gyro_mode_flag = 1;
					}
					
					//角度遥控融合
					EK_x_Gyro = XT_Gyro_x+FYL_Gyro;
					EK_y_Gyro = XT_Gyro_y+HGL_Gyro;
				}
			}
			//高度控制模式
			else if(Gyro_mode_flag == 0 && Gyro_Alt_mode_flag == 0 && Altitude_mode_flag == 0 && Direct_Alt_mode_flag == 1)
			{
				//油门限幅设定
				if(YML_pwm>=550)
						YML_pwm=550;
				
				//判断有没有起飞
				//还没起飞，置起飞标志后再做姿态控制
				if(fly_flag == 0)
				{
					//大于离地油门后做姿态控制
					if(YML_pwm>=450)
						fly_flag = 1;
				}
				//已经起飞
				else
				{
					//小于定量油门后不做姿态控制
					if(YML_pwm<=300)
						fly_flag = 0;
				}
				
				//如果油门下降, 开启下降保护
				if(YML_flag == 0)
				{
					//24ms速度控制
					//防止急速下坠
					if(wdvhc_ZSP_time == 5)
					{
						if(fly_flag == 1 && RH_AltZ_SP<fly_down)
							wdvhc_ZSP_pwm = ACC_Z_PID(fly_down, RH_AltZ_SP);
						else
							wdvhc_ZSP_pwm = 0;
					}
				}
				*/
				/*
				//还没起飞
				if(fly_flag == 0)
				{
					//起飞流程
					if(fly_out_ztj == 0)
					{
						//判断油门
						if(YML_pwm >= 500)
							fly_out_ztj = 1;
						//油门置零
						YML_pwm=0;
					}
					else if(fly_out_ztj == 1)
					{
						YML_pwm++;
						//怠速
						if(YML_pwm >= 200)
							fly_out_ztj = 2;
					}
					else if(fly_out_ztj == 2)
					{
						fly_curb_time++;
						//怠速延时3秒
						if(fly_curb_time >= 1000)
						{
							fly_out_ztj = 3;
							fly_curb_time = 0;
							//起飞油门
							YML_pwm = 300;
							//起飞标志
							fly_flag = 1;
						}
					}
				}
				else
				{
					//高度遥控融合
					EK_Alt = 500;
					if(fly_curb_time == 10)
					{
						//高度
						//位置速度获取
						NOW_Alt_SP = NOW_Alt - NOW_Alt_Last;
						NOW_Alt_Last = NOW_Alt;
						//速度环
						Alt_pwm = Alt_SPcurb(EK_Alt_SP, NOW_Alt_SP);
						
						//位移
						NOW_Dir_x_SP = NOW_Dir_x - NOW_Dir_x_Last;
						NOW_Dir_x_Last = NOW_Dir_x;
						Dir_x_Gyro = Direct_X_SPcurb(EK_Dir_x_SP, NOW_Dir_x_SP);
						
						NOW_Dir_y_SP = NOW_Dir_y - NOW_Dir_y_Last;
						NOW_Dir_y_Last = NOW_Dir_x;
						Dir_y_Gyro = Direct_Y_SPcurb(EK_Dir_y_SP, NOW_Dir_y_SP);
					}
					else if(fly_curb_time == 20)
					{
						//位置速度获取
						NOW_Alt_SP = NOW_Alt - NOW_Alt_Last;
						NOW_Alt_Last = NOW_Alt;
						
						//位置环
						EK_Alt_SP = Altitude_PID(EK_Alt, NOW_Alt);
						//速度环
						Alt_pwm = Alt_SPcurb(EK_Alt_SP, NOW_Alt_SP);
						
						EK_Dir_x_SP = Direct_X_PID(EK_Dir_x, NOW_Dir_x);
						EK_Dir_y_SP = Direct_Y_PID(EK_Dir_y, NOW_Dir_y);
						
						//位移
						NOW_Dir_x_SP = NOW_Dir_x - NOW_Dir_x_Last;
						NOW_Dir_x_Last = NOW_Dir_x;
						Dir_x_Gyro = Direct_X_SPcurb(EK_Dir_x_SP, NOW_Dir_x_SP);
						
						NOW_Dir_y_SP = NOW_Dir_y - NOW_Dir_y_Last;
						NOW_Dir_y_Last = NOW_Dir_x;
						Dir_y_Gyro = Direct_Y_SPcurb(EK_Dir_y_SP, NOW_Dir_y_SP);
					
						fly_curb_time = 0;
					}
					fly_curb_time++;
					
					//角度融合
					EK_x_Gyro = XT_Gyro_x+Dir_x_Gyro;
					EK_y_Gyro = XT_Gyro_y+Dir_y_Gyro;
				}*/
			//}
		}
		
		
		if(NOW_Alt_time >= 10)
			NOW_Alt_time = 0;
		
		//起飞后才进行姿态控制
		if(fly_flag==1)
		{
			//角速度解算
			if(time == 3)
			{
				EK_x_DG = Gyro_PID_X(EK_x_Gyro, Pitch);
				EK_y_DG = Gyro_PID_Y(EK_y_Gyro, Roll);
				EK_z_DG = Gyro_PID_Z(EK_z_Gyro, Yaw);
				
				//油门倾角补偿
				Gyro_Alt_pwm = Gyro_Alt_PID(Pitch, Roll);
			}
			
			//角速度环输出
			DGCon_x_pwm = DG_PID_X(EK_x_DG, bd_gx);
			DGCon_y_pwm = DG_PID_Y(EK_y_DG, bd_gy);
			DGCon_z_pwm = DG_PID_Z(EK_z_DG, bd_gz);
			
			//过高保护
			if(NOW_Alt > 2000)
			{
				//开启降落速度保护
				YML_flag = 0;
				twoM_Alt_pwm = 0.5f*(2000-NOW_Alt);
			}
			else
				twoM_Alt_pwm = 0;
		}
		
		//pwm融合
		pwmys = 0 +YML_pwm - DGCon_x_pwm + DGCon_y_pwm - DGCon_z_pwm + ACC_Z_pwm + twoM_Alt_pwm + Alt_pwm + Gyro_Alt_pwm;		//右上
		pwmyx = 0 +YML_pwm + DGCon_x_pwm + DGCon_y_pwm + DGCon_z_pwm + ACC_Z_pwm + twoM_Alt_pwm + Alt_pwm + Gyro_Alt_pwm;		//右下
		pwmzx = 0 +YML_pwm + DGCon_x_pwm - DGCon_y_pwm - DGCon_z_pwm + ACC_Z_pwm + twoM_Alt_pwm + Alt_pwm + Gyro_Alt_pwm;		//左下
		pwmzs = 0 +YML_pwm - DGCon_x_pwm - DGCon_y_pwm + DGCon_z_pwm + ACC_Z_pwm + twoM_Alt_pwm + Alt_pwm + Gyro_Alt_pwm;		//左上
		                                                                                                   
		//pwm限幅
		pwmys = pwmys > 999 ? 999 :(pwmys < 1 ? 1 : pwmys);
		pwmyx = pwmyx > 999 ? 999 :(pwmyx < 1 ? 1 : pwmyx);
		pwmzx = pwmzx > 999 ? 999 :(pwmzx < 1 ? 1 : pwmzx);
		pwmzs = pwmzs > 999 ? 999 :(pwmzs < 1 ? 1 : pwmzs);
		//输出
		__HAL_TIM_SetCompare(&htim1, TIM_CHANNEL_1, 1000+pwmys);    //修改比较值，修改占空比
		__HAL_TIM_SetCompare(&htim1, TIM_CHANNEL_2, 1000+pwmyx);    //修改比较值，修改占空比		这个16%油门时，其他三才动
		__HAL_TIM_SetCompare(&htim1, TIM_CHANNEL_3, 1000+pwmzx);    //修改比较值，修改占空比
		__HAL_TIM_SetCompare(&htim1, TIM_CHANNEL_4, 1000+pwmzs);    //修改比较值，修改占空比
	}
}

/***usart***/
uint8_t Rx_i = 0;
uint8_t Rx_String[24];    //接收字符串数组

uint8_t Rx_Flag = 0;      //接收字符串计数
uint8_t Rx_buff;          //接收缓存

//接收成功标志位
uint8_t Rx_ok_Flag = 0;

uint8_t ttttttttttt = 0;

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart == &huart1)
	{
		if(Rx_Flag == 0)
		{
			if(Rx_buff == 0xEF)
				Rx_Flag = 1;
			
			Rx_buff = 0;
			Rx_i = 0;
		}
		else if(Rx_Flag == 1)
		{
			if(Rx_buff == 0x0F)
				Rx_Flag = 2;
			else
				Rx_Flag = 0;
			
			Rx_buff = 0xff;
			Rx_i = 0;
		}
		else if(Rx_Flag == 2)
		{
			if(Rx_buff == 0x00)
				Rx_Flag = 3;
			else
				Rx_Flag = 0;
			
			Rx_buff = 0;
			Rx_i = 0;
		}
		else if(Rx_Flag == 3)
		{
			if(Rx_buff == 0x51)
				Rx_Flag = 4;
			else
				Rx_Flag = 0;
			
			Rx_buff = 0;
			Rx_i = 0;
		}
		else if(Rx_Flag == 4)
		{
			Rx_String[Rx_i] = Rx_buff;
			if((Rx_i == 1 && Rx_String[Rx_i] != 0x14) || ((Rx_i == 13 || Rx_i == 20 || Rx_i == 21) && Rx_String[Rx_i] != 0xFF))
			{
				Rx_i = 0;
				Rx_Flag = 0;
			}
			
			//递增
			Rx_i++;
			
			if(Rx_i == 23)
			{
				Rx_i = 0;
				Rx_Flag = 0;
				Rx_ok_Flag = 1;
				
				//光流质量
				wdvhc_GLZL = Rx_String[18];
				
				//保存上次的量
				NOW_Alt_Last = NOW_Alt;
				NOW_Dir_x_Last = NOW_Dir_x;
				NOW_Dir_y_Last = NOW_Dir_y;
				
				//高度
				NOW_DW_buff[0] = (float)((Rx_String[9] << 24)| (Rx_String[8]<<16) | (Rx_String[7]<<8) | Rx_String[6]);
				LPF_1_(3.0f,0.02f ,NOW_DW_buff[0], NOW_DW_buff_CSCS[0]);
				
				//光流
				NOW_DW_buff[1] = -(short)((Rx_String[15]<<8) | Rx_String[14]);
				NOW_DW_buff[2] = (short)((Rx_String[17]<<8) | Rx_String[16]);
				
				LPF_1_(5.0f,0.02f ,NOW_DW_buff[1],NOW_DW_buff_RHRH[1]);
				LPF_1_(5.0f,0.02f ,NOW_DW_buff[2],NOW_DW_buff_RHRH[2]);
				
				NOW_Dir_x_SP = (NOW_DW_buff_RHRH[1]*0.0076923076923076923f+gx)*130 * NOW_Alt*0.001f;
				NOW_Dir_y_SP = (NOW_DW_buff_RHRH[2]*0.0076923076923076923f+gy)*130 * NOW_Alt*0.001f;
				
				NOW_Dir_x += NOW_Dir_x_SP*0.02f;
				NOW_Dir_y += NOW_Dir_y_SP*0.02f;	
		
				NOW_Dir_x_SPLAST = 0.6f*NOW_Dir_x_SP + 0.8f*acc_sp[2];
				NOW_Dir_y_SPLAST = 0.6f*NOW_Dir_y_SP + 0.8f*acc_sp[1];
				NOW_Alt_SP = 0.5f*NOW_DW_buff_CSCS[0] + 0.7f*acc_sp[0];
				
				//判断激光有效性
				if(Rx_String[12] == 0 && NOW_Alt_Last>2300)
				{
					NOW_Alt = 2500;
					NOW_Alt_SP = 20;
				}
				
				if(ttttttttttt>=10)
				{
					acc_sp[2] = NOW_Dir_x_SPLAST;
					acc_sp[1] = NOW_Dir_y_SPLAST;
					acc_sp[0] = NOW_Alt_SP;
					ttttttttttt = 0;
				}
				ttttttttttt++;
			}
		}
		HAL_UART_Receive_IT(&huart1, (uint8_t *)&Rx_buff, 1);   //再开启；接收中断
	}
}

int fputc(int ch, FILE *f)
{ 	
	while((USART2->SR&0X40)==0); 
	USART2->DR = (uint8_t) ch;      
	return ch;
}
int fgetc(FILE *f)
{
  uint8_t ch = 0;
  HAL_UART_Receive(&huart2, &ch, 1, 0xffff);
  return ch;
}

float wdvhc_abs(float num)
{
	float xxx=0;
	if(num>=0)
		xxx = num;
	else
		xxx = -num;
	
	return xxx;
}

/***KEY****/
/*高四位下降沿判断，最低标志位*/
uint8_t Key_S0 = 0;				//PB10
uint8_t Key_S1 = 0;				//PA12
uint8_t Key_S2 = 0;				//PB14
uint8_t Key_S3 = 0;				//PB15

uint8_t Key_S0_flag = 0;
uint8_t Key_S1_flag = 0;
uint8_t Key_S2_flag = 0;
uint8_t Key_S3_flag = 0;

void key_read(void)
{
		if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_10)==0)
		{
			//前两位判断边沿
			if((Key_S0 & 0x80) == 0x80)
			{
				Key_S0 &= 0x7F;
				Key_S0 |= 0x40;
			}
			else
				Key_S0 &= 0x7F;
		}
		else
		{
			//判断下降边沿
			if((Key_S0 & 0x40) == 0x40)
			{
				//最后一位标志位
				if((Key_S0 & 0x01) == 0x01)
					Key_S0 &= 0xFE;
				else
					Key_S0 |= 0x01;
								
				Key_S0_flag = 1;
				
				//清除
				Key_S0 |= 0x80;
				Key_S0 &= 0xBF;
			}
			else
				Key_S0 |= 0x80;
		}
		
		if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_12)==0)
		{
			//前两位判断边沿
			if((Key_S1 & 0x80) == 0x80)
			{
				Key_S1 &= 0x7F;
				Key_S1 |= 0x40;
			}
			else
				Key_S1 &= 0x7F;
		}
		else
		{
			//判断下降边沿
			if((Key_S1 & 0x40) == 0x40)
			{
				//最后一位标志位
				if((Key_S1 & 0x01) == 0x01)
					Key_S1 &= 0xFE;
				else
					Key_S1 |= 0x01;
				
				Key_S1_flag = 1;
				
				//清除
				Key_S1 |= 0x80;
				Key_S1 &= 0xBF;
			}
			else
				Key_S1 |= 0x80;
		}
		
		if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_14)==0)
		{
			//前两位判断边沿
			if((Key_S2 & 0x80) == 0x80)
			{
				Key_S2 &= 0x7F;
				Key_S2 |= 0x40;
			}
			else
				Key_S2 &= 0x7F;
		}
		else
		{
			//判断下降边沿
			if((Key_S2 & 0x40) == 0x40)
			{
				//最后一位标志位
				if((Key_S2 & 0x01) == 0x01)
					Key_S2 &= 0xFE;
				else
					Key_S2 |= 0x01;
				
				Key_S2_flag = 1;
				
				//清除
				Key_S2 |= 0x80;
				Key_S2 &= 0xBF;
			}
			else
				Key_S2 |= 0x80;
		}
		
		if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_15)==0)
		{
			//前两位判断边沿
			if((Key_S3 & 0x80) == 0x80)
			{
				Key_S3 &= 0x7F;
				Key_S3 |= 0x40;
			}
			else
				Key_S3 &= 0x7F;
		}
		else
		{
			//判断下降边沿
			if((Key_S3 & 0x40) == 0x40)
			{
				//最后一位标志位
				if((Key_S3 & 0x01) == 0x01)
					Key_S3 &= 0xFE;
				else
					Key_S3 |= 0x01;
				
				Key_S3_flag = 1;
				
				//清除
				Key_S3 |= 0x80;
				Key_S3 &= 0xBF;
			}
			else
				Key_S3 |= 0x80;
		}
}

void key_read_flag(void)
{
	if(Key_S0_flag == 1)
	{

		Key_S0_flag = 0;
	}
	
	if(Key_S1_flag == 1)
	{

		Key_S1_flag = 0;
	}
	
	if(Key_S2_flag == 1)
	{

		Key_S2_flag = 0;
	}
	
	if(Key_S3_flag == 1)
	{

		Key_S3_flag = 0;
	}
}
