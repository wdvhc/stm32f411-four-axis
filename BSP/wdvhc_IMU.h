#ifndef __WDVHC_IMU_H
#define __WDVHC_IMU_H

#include "sys.h"

extern int16_t Gyro[], Acc[];			//原始数据
extern float Pitch, Roll, Yaw;		//俯仰、横滚、偏航
extern float gx, gy, gz;					//由角速度计算的角速率
extern float ax, ay, az;					//由加速度计算的加速度

extern float acc_sp[];						//积分速度
extern float acc_sp_RHRH[];				//处理后速度

extern float bd_gx, bd_gy, bd_gz;
extern float bd_ax, bd_ay, bd_az;


void ImuUpdate(float gx, float gy, float gz, float ax, float ay, float az); //g表陀螺仪，a表加计
void wdvhc_get_data(uint8_t on);
float LPF2_T2(float xin);
void LPF2_T2_Acc_DEFINE(void);
void LPF2_T2_Gyro_DEFINE(void);
void LPF2_T2_AltDir_DEFINE(void);
void LPF2_T2_gx12hz_DEFINE(void);
float LPF2_T2_JH(float xin);

#endif
